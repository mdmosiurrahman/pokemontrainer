import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonApiService } from 'src/app/services/pokemon-api.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})

export class TrainerComponent implements OnInit {

  pokemons : any = [];
  constructor(private pokeApi: PokemonApiService, private router: Router) { }

  ngOnInit(): void {
    this.findAllPokemonIds();    
  }
  //Gets all pokemon ids from local storage
  findAllPokemonIds() {
    let ids : any = [];
    let keys = Object.keys(localStorage);
    let i = keys.length;

    while ( i-- ) {
      //making sure trainer name is not being added.
      if (keys[i] != 'trainerName') ids.push(Number(localStorage.getItem(keys[i])));
    }
    this.fetchPokemons(ids);
  }

  //if card is clicked on, navigates to details of pokemon. 
  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }

  //release pokemon from localstorage if clicked on released. 
  releasePokemon(name, id) {
    localStorage.removeItem(String(id));
    alert(`${name} was released...`);
  }

  //getting pokemons from api. 
  fetchPokemons(ids){
    ids.forEach(id => {
      this.pokeApi.getPokemonByID(id).then(pmon => 
        this.pokemons.push({
          name: pmon.name,
          image: pmon.sprites.front_default,
          id: pmon.id,
          types: pmon.types
        })
      )
    });
  }
}

