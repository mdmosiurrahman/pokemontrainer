import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonApiService } from '../../../services/pokemon-api.service';

@Component({
  selector: 'app-catalouge',
  templateUrl: './catalouge.component.html',
  styleUrls: ['./catalouge.component.css']
})
 
export class CatalougeComponent implements OnInit {
  pokemons : any = [];
  indx : number = 0;
  showLoadingClass : boolean = false;

  constructor(private pokeApi: PokemonApiService, private router: Router) {}


  async ngOnInit() {
    //if the user has not yet set a trainer-name, no access to the catalouge.
    if(!localStorage.getItem('trainerName')) this.router.navigateByUrl('/');
    this.fetchPokemons(this.indx);
  }
 
  async fetchPokemons(indx) {
    await this.pokeApi.getPokemons(indx).then(res =>
      res.results.forEach(async pmon => {
        await this.pokeApi.getPokemon(pmon.url).then( pinfo =>
          //adding pokemons to list in the correct order. 
          this.insertSorted(this.pokemons,{
            id: pinfo.id,
            name: pinfo.name,
            imgsrc: pinfo.sprites.front_default,
            types: pinfo.types
          }, this.compareById)
        )
      })
    );
  }

  
  getDetails(id){
    this.router.navigateByUrl(`/details/${id}`);
  }

  //inserts to array sorted on comparator (id).
  insertSorted (array, element, comparator) {
    for (var i = 0; i < array.length && comparator(array[i], element) < 0; i++) {}
    array.splice(i, 0, element)
  }
  
  //compares pokemons by id.
  
    compareById (a, b) { 
    return a.id - b.id 
  }

  //fetching next or prev pokeons on button click, always empty array before fetch.  
  navigateNext(){
    this.indx += 50;
    this.pokemons = [];
    this.fetchPokemons(this.indx);
  }
  navigatePrev(){
    this.indx -= 50;
    this.pokemons = [];
    this.fetchPokemons(this.indx);  
  }
  
}
