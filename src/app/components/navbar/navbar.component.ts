import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    //gets the username from localstorage and displays in navbar. 
    //
    if (localStorage.getItem("trainerName")) {
      let tname = document.getElementById('trainer-name');
      tname.innerText = localStorage.getItem('trainerName');  
    }
  }

}
