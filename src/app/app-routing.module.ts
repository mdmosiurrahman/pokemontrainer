import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CatalougeComponent } from './components/pages/catalouge/catalouge.component';
import { DetailComponent } from './components/pages/detail/detail.component';
import { LandingComponent } from './components/pages/landing/landing.component';
import { TrainerComponent } from './components/pages/trainer/trainer.component';

const routes: Routes = [
  {
    path:'', 
    component: LandingComponent
  },
  {
    path:'catalouge', 
    component: CatalougeComponent
  },
  {
    path:'details/:id',
    component: DetailComponent
  },
  {
    path:'trainer',
    component: TrainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
